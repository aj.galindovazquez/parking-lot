import {
  updateParkingSpace,
  parkVehicle,
  getParkSpace,
  deleteVehicleWait,
  addParkingSpace,
} from "firebase/client";
import {useEffect, useState} from "react";
import useStateParkingSpaces from "hooks/useStateParkingSpaces";
import {
  PARKING_ESPACE_TYPE,
  VEHICLE_STATUS,
  STATE_PARKING,
  VEHICLE_TYPE,
  STATES_PROCESS,
} from "library/const";

const STATESQ={
    AVAILABLE:'Lugares disponibles',
    WAIT:'Existen lugares, sin embargo no son adecuados para su vehiculo espere por favor',
    FULL:'Sin espacios disponibles'
}
export default function useParkingNext(vechiclesQueue) {
  const [
    aviableTotal,
    aviablePSsmall,
    aviablePSmedium,
    aviablePSlarge,
  ] = useStateParkingSpaces();
 
  //////////////////////////
  const [vehicle, setVehicle] = useState(null);
  const [statusParking, setStatusParking]= useState(STATES_PROCESS.OFF)
  const [size_parking, setSizeParking]=useState(PARKING_ESPACE_TYPE.NONE)
  const [parkingSpace, setParkingSpace]=useState(null)
  const [addVehicle, setAddVehicle]= useState(STATE_PARKING.WAIT)
  const [deleteVehicleQueue, setDeleteVehicleQueue]=useState(STATES_PROCESS.OFF)
  const [checkNew, setCheckNew]=useState(false)
  
  function parkingVehicle(movil){
    console.log('Posiblee solicitud',statusParking===STATES_PROCESS.OFF)
    if(movil !== null)
      if(statusParking===STATES_PROCESS.OFF){
        setVehicle(movil)
      }
  }
  
  useEffect(()=>{
    console.log('Vehiculo', vehicle)
    if(vehicle !== null){
      console.log('1. Vehiculo', vehicle)
      setStatusParking(STATE_PARKING.INITIAL)
    }else{
      console.log('POOOOOO')
      setStatusParking(STATES_PROCESS.OFF)
    }
  },[vehicle]);


  useEffect(()=>{
    if(statusParking===STATE_PARKING.INITIAL){
      console.log('Iniciando proceso', size_parking)
      if(size_parking===PARKING_ESPACE_TYPE.NONE){
        if(vehicle !== null)
        {
          const sp= checkAvailableParkingSpaces(vehicle.type)
          if(sp === PARKING_ESPACE_TYPE.NONE)
            setStatusParking(STATES_PROCESS.OFF)
          else
            setSizeParking(sp)
        }
       
      }
    }else if(statusParking===STATE_PARKING.COMPLETE){
      setAddVehicle(STATE_PARKING.COMPLETE)
    }else if(statusParking===STATE_PARKING.WAIT){
      updateParkingSpace(parkingSpace.id,false,vehicle.type,setStatusParking)
    }else if(statusParking===STATE_PARKING.FINISH){
      setStatusParking(STATES_PROCESS.OFF)
      setCheckNew(true)
    }

    if(statusParking===STATES_PROCESS.OFF){
      setCheckNew(false)
      setSizeParking(PARKING_ESPACE_TYPE.NONE)
      setParkingSpace(null)
      setAddVehicle(STATE_PARKING.WAIT)
      setDeleteVehicleQueue(STATE_PARKING.WAIT)
    }
  },[statusParking]);



  useEffect(()=>{
    if(statusParking===STATE_PARKING.INITIAL)
    {
      console.log('2. SizeParking', size_parking) 
      if(size_parking !== null){
        getParkSpace(size_parking, true,setParkingSpace)
      }else{
        setStatusParking(STATES_PROCESS.OFF)
      }
    }
  },[size_parking]);

  useEffect(()=>{
    if(statusParking===STATE_PARKING.INITIAL)
      if(parkingSpace !== null && parkingSpace){
        console.log('3. Parking Space', parkingSpace)
          setStatusParking(STATE_PARKING.WAIT)
      }else{
        setStatusParking(STATES_PROCESS.OFF)
      }
  },[parkingSpace]);

  useEffect(()=>{
    if(statusParking===STATE_PARKING.COMPLETE)
      if(addVehicle === STATE_PARKING.COMPLETE){
        console.log('4. Agregado Completo')
        deleteVehicleWait(vehicle.id,setDeleteVehicleQueue)
      }else{
        setStatusParking(STATES_PROCESS.OFF)
      }
  },[addVehicle]);

  useEffect(()=>{
    if(statusParking===STATE_PARKING.COMPLETE)
      if(deleteVehicleQueue === STATE_PARKING.COMPLETE){
        console.log('5. Borrado .. terminado')
        setStatusParking(STATES_PROCESS.OFF)
      }else{
        setStatusParking(STATES_PROCESS.OFF)
      }
  },[deleteVehicleQueue]);

  function checkAvailableParkingSpaces(type_vehicle) {
    console.log("VEHICLE", type_vehicle);
    switch (type_vehicle) {
      case VEHICLE_TYPE.MOTORCICLE:
          return aviablePSsmall
          ? PARKING_ESPACE_TYPE.SMALL
          : aviablePSmedium
          ? PARKING_ESPACE_TYPE.MEDIUM
          : aviablePSlarge
          ? PARKING_ESPACE_TYPE.LARGE
          : PARKING_ESPACE_TYPE.NONE;
        break;
      case VEHICLE_TYPE.SEDAN:
        return aviablePSmedium
          ? PARKING_ESPACE_TYPE.MEDIUM
          : aviablePSlarge
          ? PARKING_ESPACE_TYPE.LARGE
          : PARKING_ESPACE_TYPE.NONE;
        break;
      case VEHICLE_TYPE.TRUCK:
         return aviablePSlarge
          ? PARKING_ESPACE_TYPE.LARGE
          : PARKING_ESPACE_TYPE.NONE
        break;
      default:
        return null 
        break;
    }
  }

  return [parkingVehicle, checkNew];
}
