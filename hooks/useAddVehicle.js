import { addVehicle} from "firebase/client";
import {useEffect, useState} from "react";
   
  export default function useAddVehicle() {
    const [vehicleNew, setVehicleNew] = useState(null);
    
    useEffect(() => {
      // Nuevo automvil
      //Caso de que ya se tenga un automovil en espera o sin lugares disponibles se agrega a la cola
      if (vehicleNew !== null) {
          addVehicle({type: vehicleNew})
            .then(() => {
              setVehicleNew(null)
                //Verificar si existen espacios disponibles
            })
            .catch((err) => {
              console.error(err);
            });
      }
    }, [vehicleNew]);
  
    return [setVehicleNew];
  }
  