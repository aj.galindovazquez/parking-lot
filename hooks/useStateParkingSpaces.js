import { getPSpacesAvaliablesLarge, getPSpacesAvaliablesMedium, getPSpacesAvaliablesSmall, getTotalParkingSpaces } from "firebase/client";
import { PARKING_ESPACE_TYPE } from "library/const";
import { useState, useEffect } from "react";

export default function useStateParkingSpaces(){
const [aviablePSsmall, setAviablePSsmall]=useState(0)
const [aviablePSmedium, setAviablePSmedium]=useState(0)
const [aviablePSlarge, setAviablePSlarge]=useState(0)
const [aviableTotal, setAviableTotal]=useState(0)
useEffect(() => {
    let small=getTotalParkingSpaces(true, PARKING_ESPACE_TYPE.SMALL,setAviablePSsmall)
    let medium=getTotalParkingSpaces(true, PARKING_ESPACE_TYPE.MEDIUM,setAviablePSmedium)
    let large=getTotalParkingSpaces(true, PARKING_ESPACE_TYPE.LARGE,setAviablePSlarge)
}, []);

useEffect(()=>{
    setAviableTotal(aviablePSlarge+aviablePSmedium+aviablePSsmall)
},[aviablePSsmall, aviablePSmedium, aviablePSlarge])

    return[aviableTotal,aviablePSsmall, aviablePSmedium, aviablePSlarge]
}