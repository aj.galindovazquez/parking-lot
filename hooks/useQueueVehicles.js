import { getVehiclesList } from "firebase/client";
import { useEffect, useState } from "react";
import useStateParkingSpaces from "./useStateParkingSpaces";

export default function useQueueVehicles(parkingVehicle,checkQueue){
    const [vechiclesQueue, setVehiclesQueue]=useState([])
    const [
      aviableTotal,
      aviablePSsmall,
      aviablePSmedium,
      aviablePSlarge,
    ] = useStateParkingSpaces();

    const [length,setLength]= useState(0)
    useEffect(() => {
        let vehicles=getVehiclesList(setVehiclesQueue)
        setLength(setVehiclesQueue.length)
      }, []);

    useEffect(() => {
      console.log('Cambio la cola')
      setTimeout(() => {
        console.log('solicitud')
        if(vechiclesQueue.length>0){
          parkingVehicle(vechiclesQueue[0])
        }
      }, 1000);
    }, [vechiclesQueue]);

  return[vechiclesQueue]
}