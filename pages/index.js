import Button from "react-bootstrap/Button";
import styles from "../styles/Home.module.css";
import {PARKING_ESPACE_TYPE, VEHICLE_STATUS,STATE_PARKING, VEHICLE_TYPE} from "library/const";
import ParkingSpacesList from "components/Parking/ParkingSpacesList";
import Header from "components/Header";
import { ParkingState } from "components/Parking/ParkingState";
import VehicleAddModal from "components/Vehicle/VehicleAddModal";
import VehicleList from "components/Vehicle/VehicleList";
import {getParkingSpaces} from "firebase/client"
import { useEffect, useState } from "react";
import useStateParkingSpaces from "hooks/useStateParkingSpaces";
import useQueueVehicles from "hooks/useQueueVehicles";
import ParkingSpacesAdmin from "components/Parking/ParkingSpacesAdmin";
import useAddVehicle from "hooks/useAddVehicle";
import useParkingNext from "hooks/useParkingNext";


export default function Home(){
  
  const [checkQueue,setCheckQueue]=useState(false)
  const [aviableTotal,aviablePSsmall, aviablePSmedium, aviablePSlarge]=useStateParkingSpaces()
  const [parkingSpaces, setParkingSpaces]=useState([])
  const [parkingVehicle,checkNew]= useParkingNext([])
  const [vechiclesQueue]=useQueueVehicles(parkingVehicle,checkQueue)
  const [showAdminSpaces, setShowAdminSpaces]=useState(false)
  

  const [setVehicleNew]= useAddVehicle()
  useEffect(() => {
    let parkings=getParkingSpaces(setParkingSpaces)
  }, []);

  useEffect(()=>{
    setTimeout(() => {
      if(vechiclesQueue.length>0)
      parkingVehicle(vechiclesQueue[0])
    }, 1000);
  },[parkingSpaces]);
  
  return (
    <>
      <Header />
      <div className="container mt-3">
        <div className="row">
          <div className="col-sm-12 col-md-7">
              <ParkingSpacesList list={parkingSpaces} />
          </div>
          <div className="col-sm-12 col-md-5">
            <div className='row'>
              <div className='col-12 allCenter'>
              <Button variant="dark" onClick={()=> setShowAdminSpaces(true)}>Administrar espacios del estacionamieto</Button>
              </div>
              <div className='col-12 allCenter'>
              <ParkingState total={aviableTotal}  small={aviablePSsmall} medium={aviablePSmedium} large={aviablePSlarge} />
              </div>
              <div className='col-12 allCenter'>
              <VehicleAddModal handleClick={setVehicleNew}/>
              </div>
            </div>
            <VehicleList list={vechiclesQueue}/>
          </div>
        </div>
      </div>
      <ParkingSpacesAdmin
            show={showAdminSpaces}
             handleClose={()=>setShowAdminSpaces(false)}
             handleShow={()=>setShowAdminSpaces(true)}
             aviableTotal={aviableTotal}
             aviablePSsmall={aviablePSsmall}
             aviablePSmedium={aviablePSmedium}
             aviablePSlarge={aviablePSlarge}
            />
    </>
  );
}
