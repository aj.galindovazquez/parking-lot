// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
  res.statusCode = 200
  res.json([
    { id: '1', name: 'shirt', category: 'clothing' },
    { id: '2', name: 'Sports Tennis', category: 'accessories' },
    { id: '3', name: 'Casual shoes', category: 'footwear' },
    { id: '4', name: 'skirt', category: 'clothing' },
    { id: '5', name: 'tie', category: 'clothing' },
    { id: '1', name: 'shirt', category: 'clothing' },
    { id: '2', name: 'Sports Tennis', category: 'accessories' },
    { id: '3', name: 'Casual shoes', category: 'footwear' },
    { id: '4', name: 'skirt', category: 'clothing' },
    { id: '5', name: 'tie', category: 'clothing' }
  ])
}
