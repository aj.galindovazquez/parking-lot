import { PARKING_ESPACE_TYPE } from "library/const"
import { colors } from "./theme"

export function getColorParkingSpaceType(parkingSpaceType){
    switch (parkingSpaceType) {
        case PARKING_ESPACE_TYPE.SMALL:
            return colors.small
        case PARKING_ESPACE_TYPE.MEDIUM:
                return colors.medium
        case PARKING_ESPACE_TYPE.LARGE:
                    return colors.large
    }
}