export const breakpoints = {
  mobile: "520px",
};

export const fonts = {
  base:
    'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu, "Helvetica Neue", sans-serif',
};

export const colors = {
  black: "#000000",
  white: "#ffffff",
  primary: "#0099ff",
  secondary: "#1c5480",
  silver:"#C0C0C0",
  small:"#D50F25",
  medium:"#EEB211",
  large:"#009925"
};

export const border = {
  radius: {
    rounded: '5px',
    circle: '9999px',
    none: '0px'
  }
};
