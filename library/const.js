export const VEHICLE_TYPE={
    MOTORCICLE:'Motocicleta',
    SEDAN:'Automóvil',
    TRUCK:'Camión'
}
export const STATE_PARKING={
    INITIAL:'0',
    WAIT:'1',
    PROCESS:'2',
    PARKING:'5',
    COMPLETE:'4',
    FINISH:'8',
    ERROR:'3'
  }

export const STATES_PROCESS={
    OFF:'-2',
    WAIT:'1',
    INITIAL:'0',
    PROCESS:'2',
    COMPLETE:'4',
    ERROR:'3'
}



export const VEHICLE_STATUS={
    WAIT:'wait',
    PARKED:'parked',
    FINISHED:'finished'
}
export const PARKING_ESPACE_TYPE={
    SMALL:'Pequeño',
    MEDIUM:'Mediano',
    LARGE:'Grande',
    NONE:'No disponible'
}