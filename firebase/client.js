import firebase from "firebase";
import { PARKING_ESPACE_TYPE,STATES_PROCESS,STATE_PARKING, VEHICLE_STATUS } from "library/const";
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBGW1At2Ip6kCer0kuRLaiKtcp37kgKr8E",
    authDomain: "devter-e8146.firebaseapp.com",
    databaseURL: "https://devter-e8146.firebaseio.com",
    projectId: "devter-e8146",
    storageBucket: "devter-e8146.appspot.com",
    messagingSenderId: "709036871915",
    appId: "1:709036871915:web:fe037744c756c06eae6eae",
    measurementId: "G-KWHWC41J22"
  };
  !firebase.apps.length && firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();
  
  export const addParkingSpace = ({size}) => {
    const available = true;
    const vehicleType='';
    return db.collection("parking_spaces").add({
      available,
      vehicleType,
      size
    });
  };

  export const addVehicle = ({ type }) => {
    const status=VEHICLE_STATUS.WAIT
    return db.collection("vehicles_queue").add({
      type,
      status,
      createdAt: firebase.firestore.Timestamp.fromDate(new Date()),
    });
  };

  export const getVehiclesList = (callback) => {
    console.log('solicitando', status)
    return db
      .collection("vehicles_queue")
      .orderBy("createdAt", "asc")
      .limit(20)
      .onSnapshot(({ docs }) => {
        const newVehicles = docs.map(mapVehiclesFromFirebaseToDevitObject)
        callback(newVehicles.length>0?newVehicles:[])
      })
  }

  export const parkVehicle=(size, callback)=>{
    return db
    .collection("parking_spaces")
    .where("available","==",true)
    .where("size","==",size)
    .limit(1)
    .onSnapshot(({ docs }) => {
      const newParkings = docs.map(mapParkingSpacesFromFirebaseToDevitObject)
      callback(newParkings[0])
    })
  }

  export const getParkSpace=(size,available,callback)=>{
    return db
    .collection("parking_spaces")
    .where("available","==",available)
    .where("size","==",size)
    .onSnapshot(({ docs }) => {
      const newParkings = docs.map(mapParkingSpacesFromFirebaseToDevitObject)
      callback(newParkings[0])
    })
  }

  export const updateParkingSpace=(id,available, vehicleType,callback)=>{
    console.log('Actualizando ...',id,available,vehicleType)

    var objectRef= db.collection("parking_spaces").doc(id);
    objectRef.update({
      'available':available,
      'vehicleType':vehicleType
    }).then(function() {
        callback(STATES_PROCESS.COMPLETE)
        console.log("Document successfully updated!");
    }).catch(function(error) {
        callback(STATE_PARKING.ERROR)
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
  }

  export const deleteVehicleWait=(id, callback)=>{
    var vQ=db.collection("vehicles_queue").doc(id)
    vQ.delete().then(function() {
      callback(STATES_PROCESS.COMPLETE)
         console.log("Eliminado!");
    }).catch(function(error) {
      callback(STATES_PROCESS.ERROR)
        console.error("Error delete document: ", error);
    });
  }
  export const deleteParkingSpace=(id,callback)=>{
        var vQ=db.collection("parking_spaces").doc(id)
        vQ.delete().then(function() {
          console.log("Espacio eliminado!");
          callback(STATES_PROCESS.COMPLETE)
        }).catch(function(error) {
          console.error("Error delete document: ", error);
          callback(STATES_PROCESS.ERROR)
        });
  }


  export const getParkingSpaces = (callback) => {
    return db
      .collection("parking_spaces")
      .orderBy("size", "desc")
      .onSnapshot(({ docs }) => {
        const newParkingSpaces = docs.map(mapParkingSpacesFromFirebaseToDevitObject)
        callback(newParkingSpaces)
      })
  }
  
  export const getTotalParkingSpaces = (value,size,callback) => {
    return db
      .collection("parking_spaces")
      .where("available","==",value)
      .where("size","==",size)
      .onSnapshot(({ docs }) => {
        callback(docs.length)
      })
  }

  const mapVehiclesFromFirebaseToDevitObject = (doc) => {  
    const data = doc.data()
    const id = doc.id  
      return {
        ...data,
        id,
      }
    
    return null
  }
  const mapParkingSpacesFromFirebaseToDevitObject = (doc) => {
    const data = doc.data()
    const id = doc.id  
    return {
      ...data,
      id,
    }
  }

 
