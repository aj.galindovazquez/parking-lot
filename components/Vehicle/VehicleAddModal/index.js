import { VEHICLE_TYPE } from "library/const";
import VehicleButton from "../VehicleButton";

export default function VehicleAddModal({handleClick}){
    return(
        <>
        <main>
            <div className='container'>
            <p className="p-0 m-0 fw-light fst-italic">
            Agregar nuevos vehiculos
          </p>
                <div className='row mt-2'>
                    <div className='col'>
                        <VehicleButton vehicleType={VEHICLE_TYPE.TRUCK} onClick={handleClick}/>
                    </div>
                    <div className='col'>
                        <VehicleButton vehicleType={VEHICLE_TYPE.SEDAN} onClick={handleClick}/>
                    </div>
                    <div className='col'>
                        <VehicleButton vehicleType={VEHICLE_TYPE.MOTORCICLE} onClick={handleClick}/>
                    </div>

                </div>

            </div>
        </main>
        </>
    )
}