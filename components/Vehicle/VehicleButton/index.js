import { Button } from "react-bootstrap";
import {colors} from "styles/theme";import VehicleIcon from "../VehicleIcon";

export default function VehicleButton({vehicleType, onClick}){

    return(
        <>
        <section>
        <div className='allCenter'>
        <Button className='rounded-circle allCenter shadow-sm' variant="outline-primary" onClick={()=>onClick(vehicleType)}>
            <VehicleIcon vehicleType={vehicleType} color={colors.primary} size={'large'}/>
        </Button>
        </div>
        <strong>{vehicleType}</strong>
        </section>
        <style jsx>
            {`
            section{
                text-align:center;
            }
            `}

        </style>
        </>
    )

}