import {colors} from "styles/theme";import VehicleIcon from "../VehicleIcon";

export default function VehicleItem({vehicleType,wait, position}){
    return(
        <>
         <main className='rounded p-1'>
             <section className='d-flex justify-content-center'>
             <div className='rounded-circle bg-light allCenter'>
                <VehicleIcon vehicleType={vehicleType} color={colors.black} size={'medium'} hover={false} />
             </div>
             </section>
             <small>{vehicleType}</small>
         </main>
         <style jsx>
             {`
             main{
                 text-align:center;
                 background: ${wait ? colors.secondary : colors.silver};
                 color: ${colors.white};
                 width: 70px;
                 height: 100%;
             }
             div{
                 width: 35px;
                 height: 35px;
             }
             `}
         </style>
        </>
    )
}