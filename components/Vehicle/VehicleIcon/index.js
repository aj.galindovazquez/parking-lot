import {VEHICLE_TYPE} from "library/const";
import {colors} from "styles/theme";
import {FaMotorcycle, FaCarSide, FaTruckMoving} from "react-icons/fa";

export default function VehicleIcon({vehicleType, size, color, hover}) {
  return (
    <>
      {vehicleType === VEHICLE_TYPE.MOTORCICLE && (
        <em>
          {" "}
          <FaMotorcycle />{" "}
        </em>
      )}
      {vehicleType === VEHICLE_TYPE.SEDAN && (
        <em>
          {" "}
          <FaCarSide />{" "}
        </em>
      )}
      {vehicleType === VEHICLE_TYPE.TRUCK && (
        <em>
          {" "}
          <FaTruckMoving />{" "}
        </em>
      )}
      <style jsx>
        {`
          em {
            color: ${color || colors.white};
            font-size: ${size || 'xx-large'};
          }
          em:hover{
              color:${hover ? colors.white : color || colors.white};
          }
        `}
      </style>
    </>
  );
}
