import {VEHICLE_STATUS} from "library/const";
import {useState} from "react";
import VehicleItem from "../VehicleItem";

export default function VehicleList({list, vehicleWait}) {
  const first=list.length>0?list[0]:null
  return (
    <section className="d-flex justify-content-center w-100">
      <div className="row allCenter">
        {list.map(({type, id, status}) => {
          if (status === VEHICLE_STATUS.WAIT) {
            return (
              <div className="col-auto p-0 m-1" key={id}>
                <VehicleItem wait={first.id===id} vehicleType={type} />
              </div>
            );
          } else {
            console.log("nada");
          }
        })}
      </div>
    </section>
  );
}
