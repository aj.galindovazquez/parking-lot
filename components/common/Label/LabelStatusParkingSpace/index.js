import { ParkingState } from "components/Parking/ParkingState";
import { colors } from "styles/theme";
import { getColorParkingSpaceType } from "styles/utils";

export default function LabelStatusParkingSpace({number, parkingSpaceType}){
    return(
        <>
        <main>
            <section className='rounded-circle allCenter'>
                {number}
            </section>
            <strong>{parkingSpaceType}</strong>
        </main>
        <style jsx>
            {`
            section{
                width: 55px;
                height: 55px;
                font-size:large;
                font-weight:bolder;
                border: 2px solid ${colors.secondary};
            }
            `}
        </style>
        </>
    )
}