import React from 'react'
import { border } from 'styles/theme'
import { colors } from 'styles/theme'


export default function ButtonCircle ({ id,children, onClick, disabled }) {
  const handleSubmit = (event) => {
    event.preventDefault()
    onClick()
  }
  return (
    <>
      <button disabled={disabled} onClick={()=>onClick()} id={id} data-testid={id} className="allCenter">
        {children}
      </button>

      <style jsx>{`
        button {
          background: ${colors.white};
          border-radius: ${border.radius.rounded};
          border: solid 1px ${colors.silver};
          color: ${colors.black};
          cursor: pointer;
          width:100%;
          height:35px;
          user-select: none;
        }
        button:hover {
          opacity: 0.7;
        }
        button[disabled] {
          opacity: 0.2;
        }
      `}</style>
    </>
  )
}