export default function ContainerOverlappingOver ({ children, center, width, height }) {
    return (
        <>
        <main className={center ? 'allCenter' : ''}>
              <div >
                      {children}
              </div>
        </main>
  
        <style jsx>
          {`
            div {
              position: absolute;
              z-index:1;
              text-align:${center ? 'center' : 'auto'}
            }
            main{
              width: ${width};
              height: ${height};
            }
          `}
        </style>
        </>
    )
  }