import React from 'react'
export default function ContainerOverlappingUnder ({ children, center, width, height }) {
  return (
      <>
      <div>
            {children}
      </div>
      <style jsx>
        {`
          div {
            width: ${width};
            height: ${height};
            position: absolute;
            z-index:0;
            text-align:${center ? 'center' : 'auto'}
          }
        `}
      </style>
      </>
  )
}
