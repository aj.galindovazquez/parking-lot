import { Navbar } from "react-bootstrap";

export default function Header(){
    return(
        <Navbar bg="light" variant="light">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="/logo.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          Estacionamiento
        </Navbar.Brand>
      </Navbar>
    )
}