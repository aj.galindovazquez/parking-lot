import {Badge, Button, Modal} from "react-bootstrap";
import {getColorParkingSpaceType} from "styles/utils";
import {colors} from "styles/theme";
import VehicleIcon from "components/Vehicle/VehicleIcon";
import {AiOutlineArrowRight} from 'react-icons/ai'
import { border } from "styles/theme";
import { updateParkingSpace } from "firebase/client";
import { useEffect, useState } from "react";
import { STATE_PARKING } from "library/const";
export default function ParkingSpace({
  id,
  available,
  licensePlate,
  size,
  vehicleType,
}) {
  const height = "100px";
  const width = "70px";
  const [showOutsideVehicle, setShowOutsideVehicle] = useState(false);
  const [statusProcess, setStatusProcess]=useState(STATE_PARKING.INITIAL)
  const handleClose = () => setShowOutsideVehicle(false);
  const handleShow = () => setShowOutsideVehicle(true);

  useEffect(() => {
    console.log('stado',statusProcess)
    if(statusProcess=== STATE_PARKING.COMPLETE)
    {
      handleClose()
      setStatusProcess(STATE_PARKING.INITIAL)
    }
  }, [statusProcess]);
  function outsideVehicle(){
    updateParkingSpace(
      id,
      true,
      '',
      setStatusProcess
    );
  }
  return (
    <>
      <main className='allCenter'>
        <section >
          <Badge variant="light">{size}</Badge>
          <div className="container ">
            <div className="row ">
              <div className="col">
                {!available && 
                 <VehicleIcon vehicleType={vehicleType}/>
                }
                {!available && 
                  <button onClick={()=> handleShow()}> <AiOutlineArrowRight/> </button>
                }
              </div>
            </div>
          </div>
        </section>
        </main>
        
      <Modal show={showOutsideVehicle} onHide={handleClose}>
        <Modal.Header closeButton className='m-0 p-1 border-0'>
          <Modal.Title></Modal.Title>
        </Modal.Header>
        <Modal.Body className='m-0 p-0 allCenter'>¿Estás seguro de <strong className='m-1'> salir </strong> del estacionamiento?</Modal.Body>

        <Modal.Footer className='border-0 allCenter m-1 p-0'>
          <Button variant="secondary" onClick={handleClose}>
            No
          </Button>
          <Button variant="primary" onClick={outsideVehicle}>
            Si
          </Button>
        </Modal.Footer>
      </Modal>

      <style jsx>
        {`
          section {
            height: ${height};
            width: ${width};
            text-align:center;
            background: ${available ? colors.primary : colors.silver};
            opacity: ${available ? ".9" : ".6"};
          }
          main{
            height: ${height};
            width: ${width};
          }
          button{
          background: ${colors.white};
          border-radius: ${border.radius.circle};
          border: solid 1px ${colors.silver};
          color: ${colors.primary};
          cursor: pointer;
          font-size:small;
          font-weight:bolder;
          width:25px;
          height:25px;
          }
          
          small {
            color: ${colors.white};
          }
        `}
      </style>
    </>
  );
}
