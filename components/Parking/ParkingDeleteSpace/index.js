import ButtonCircle from "components/common/Buttons/ButtonCircle";
import { addParkingSpace, deleteParkingSpace, getParkSpace } from "firebase/client";
import { PARKING_ESPACE_TYPE, STATES_PROCESS } from "library/const";
import { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";

export default function ParkingDeleteSpace({size}) { 
    const [processDelete, setProcessDelete]=useState(STATES_PROCESS.OFF)
    const [idDelete, setIdDelete]=useState(null)
    
    useEffect(() => {
        if(processDelete===STATES_PROCESS.INITIAL)
        {
            setProcessDelete(STATES_PROCESS.WAIT)
            getParkSpace(size,true,setIdDelete)
        }
        if(processDelete===STATES_PROCESS.PROCESS)
        {
            console.log('Proceso', processDelete)
            deleteParkingSpace(idDelete.id,setProcessDelete)
        }
        if(processDelete===STATES_PROCESS.COMPLETE){
            setProcessDelete(STATES_PROCESS.OFF)
            setIdDelete(null)
        }
    }, [processDelete]);

    useEffect(() => {
        console.log('IDelimnar', idDelete)
        if(idDelete !== null){
            setProcessDelete(STATES_PROCESS.PROCESS)
        }
    }, [idDelete]);
    
    return (
      <>
      <ButtonCircle
      onClick={()=>setProcessDelete(STATES_PROCESS.INITIAL)}
      disabled={processDelete!==STATES_PROCESS.OFF}
      >
          Elimar un espacio {size}
      </ButtonCircle>
      </>
    );
  }