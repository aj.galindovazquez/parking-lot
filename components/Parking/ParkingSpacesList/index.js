import ParkingSpace from "components/Parking/ParkingSpace";

export default function ParkingSpacesList({list}){
    return(
        <>
        <section className='d-flex justify-content-center w-100'>
        <div className='row allCenter'>
        {list.map(
            ({
            id,
            available,
            size,
            vehicleType
            }) => {
              return (
                  <div className='col-auto p-0 m-1' key={id}> 
                    <ParkingSpace
                    id={id}
                    available={available}
                    size={size}
                    vehicleType={vehicleType}
                    />
                </div>
              )
            }
        )}
        </div>
        </section>
        </>
    )
}