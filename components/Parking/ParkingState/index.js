import LabelStatusParkingSpace from "components/common/Label/LabelStatusParkingSpace";
import {PARKING_ESPACE_TYPE, VEHICLE_TYPE} from "library/const";
import {Alert, Badge} from "react-bootstrap";
import {FaMedium} from "react-icons/fa";

export function ParkingState({total, available, small, medium, large}) {
  return (
    <>
      <main className="m-2">
        <div className="container border shadow-sm">
          <p className="p-0 m-0 fw-light fst-italic">
            Espacios disponibles:
          </p>
          {total > 0 && (
            <div className="row m-2">
              <div className="col">
                <LabelStatusParkingSpace
                  number={small}
                  parkingSpaceType={PARKING_ESPACE_TYPE.SMALL}
                />
              </div>
              <div className="col">
                <LabelStatusParkingSpace
                  number={medium}
                  parkingSpaceType={PARKING_ESPACE_TYPE.MEDIUM}
                />
              </div>
              <div className="col">
                <LabelStatusParkingSpace
                  number={large}
                  parkingSpaceType={PARKING_ESPACE_TYPE.LARGE}
                />
              </div>
            </div>
          )}
          {total === 0 && (
            <Alert variant={"danger"} className="allCenter">
              <strong>Agotados</strong>
            </Alert>
          )}
        </div>
      </main>
      <style jsx>
        {`
          main {
            min-width:200px;
          }
        `}
      </style>
    </>
  );
}
