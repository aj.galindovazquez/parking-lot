import { addParkingSpace, deleteParkingSpace, getParkSpace } from "firebase/client";
import { PARKING_ESPACE_TYPE, STATES_PROCESS } from "library/const";
import { useEffect, useState } from "react";
import { Alert, Button, Modal } from "react-bootstrap";
import ParkingDeleteSpace from "../ParkingDeleteSpace";

const ACTIONS={
    DELETE_FREE_SPACES:'1',
    DELETE_BUSY_SPACES:'3',
    ADD:'2',
    NONE:'4'
}
export default function ParkingSpacesAdmin({
    handleClose,
    show,
    aviableTotal,aviablePSsmall, aviablePSmedium, aviablePSlarge
}) { 
    const [action, setAction]=useState(ACTIONS.NONE)
    function addParkingSpaceFunction(size){
            addParkingSpace({size:size})
            .then(() => {
              console.log('se guardo')
            })
            .catch((err) => {
              console.error(err);
            });
      } 
    return (
      <>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton className='m-1 p-0 border-0'>
            <Modal.Title>Administrar espacios del estacionamiento</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <div className='allCenter'>
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-primary" onClick={()=> setAction(ACTIONS.ADD)}>Agregar espacios</button>
                <button type="button" class="btn btn-primary" onClick={()=> setAction(ACTIONS.DELETE_FREE_SPACES)} >Eliminar espacios disponibles</button>
                {/* <button type="button" class="btn btn-primary" onClick={()=> setAction(ACTIONS.DELETE_BUSY_SPACES)}>Eliminar espacios ocupados</button> */}
            </div>
          </div>
          { action=== ACTIONS.ADD &&
                <div className='row p-3'>
                <div className='col-12 m-1 allCenter'>
                    <Button className='rounded' variant="outline-secondary" block onClick={()=>addParkingSpaceFunction(PARKING_ESPACE_TYPE.LARGE)}>Agregar espacio {PARKING_ESPACE_TYPE.LARGE}</Button>
                </div>
                <div className='col-12 m-1 allCenter'>
                    <Button className='rounded' variant="outline-secondary" block onClick={()=>addParkingSpaceFunction(PARKING_ESPACE_TYPE.MEDIUM)}>Agregar espacio {PARKING_ESPACE_TYPE.MEDIUM}</Button>
                </div>
                <div className='col-12 m-1 allCenter'>
                    <Button className='rounded' variant="outline-secondary" block onClick={()=>addParkingSpaceFunction(PARKING_ESPACE_TYPE.SMALL)}> Agregar espacio {PARKING_ESPACE_TYPE.SMALL}</Button>
                </div>
            </div>
          }
          { action=== ACTIONS.DELETE_FREE_SPACES &&
            <div className='row p-3'>
              <div className='col-12 m-1 allCenter'>
              {aviablePSlarge>0 &&
                <ParkingDeleteSpace size={PARKING_ESPACE_TYPE.LARGE}/>
              }
              </div>
              <div className='col-12 m-1 allCenter'>
              {aviablePSmedium>0 &&
              <ParkingDeleteSpace size={PARKING_ESPACE_TYPE.MEDIUM}/>
              }
              </div>
              <div className='col-12 m-1 allCenter'>
              {aviablePSsmall>0 &&
              <ParkingDeleteSpace size={PARKING_ESPACE_TYPE.SMALL}/>
                }
              </div>
          </div>
          }
          { action=== ACTIONS.NONE &&
          <Alert variant='info' className='m-3'>
              Los espacios se cambiaran automaticamente en el estacionamiento,
              solo se pueden borrar los espacios que no se encuentren ocupados 
              por un vehiculo.
          </Alert>

          }
          
          
          
          <Modal.Footer>
            <p>< strong className='ml-2'>Total:</strong>{aviableTotal} <strong className='ml-2'>{PARKING_ESPACE_TYPE.LARGE}:</strong>{aviablePSlarge} <strong className='ml-2'>{PARKING_ESPACE_TYPE.MEDIUM}:</strong>{aviablePSmedium} <strong className='ml-2'>{PARKING_ESPACE_TYPE.SMALL}:</strong>{aviablePSsmall}</p>
          </Modal.Footer>
          
          </Modal.Body>
        </Modal>
      </>
    );
  }
